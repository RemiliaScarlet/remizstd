#!/bin/bash

set -e

(
    cd www
    echo "Running Pandoc"
    pandoc --css template-dark.css --template template.html --toc -s --highlight-style zenburn -f markdown -t html -o remizstd.html remizstd.md
    #echo "Adjusting"
    #sed -r -i 's/max-width: [0-9]+em/max-width: 1024px/' remizstd.html
)
echo "*** Docs built ***"
