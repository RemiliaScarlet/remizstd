# Package

version       = "0.1.1"
author        = "MistressRemilia"
description   = "Bindings for ZStandard"
license       = "GPL-3.0-or-later"
srcDir        = "src"

# Dependencies

requires "nim >= 1.6.0"
