# remizstd - Nim bindings for ZStandard, based on zstd.cr
# Copyright (C) 2022 Remilia Scarlet <remilia@posteo.jp>
# Copyright (C) 2019-2021 Didactic Drunk
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

## The remizstd module implements bindings for the ZStandard library.
##
## These bindings are essentially a port of the Crystal bindings by Didactic
## Drunk.  The Crystal bindings can be found here:
## https://github.com/didactic-drunk/zstd.cr
##
## Features
## --------
## - Two APIs: a simple context-based one, and one for (de)compressing to/from streams.
## - API calls allow reusable buffers to reduce GC overhead.
## - `export ZSTD_CLEVEL=1` sets the default compression level just like the zstd command line utilities.
## - Probably pretty fast
## - Custom dictionaries (not well tested)
##
## To import all of the library (minus the low-level FFI stuff), use `import
## remizstd`.  Or, you can import just what you need:
## - `remizstd/common <remizstd/common.html>`_: Custom dictionaries and some common types/methods/values
## - `remizstd/compress <remizstd/compress.html>`_: Context-based compression
## - `remizstd/decompress <remizstd/decompress.html>`_: Context-based decompression
## - `remizstd/compstream <remizstd/compstream.html>`_: Compressing streams
## - `remizstd/decompstream <remizstd/decompstream.html>`_: Decompression streams
## - `remizstd/libffi <remizstd/libffi.html>`_: Low-level C bindings
runnableExamples:
  import remizstd/[compress, decompress]

  let buf = "this is a test buffer"

  # Compression using a context
  var
    cctx = newCompressCtx(1) # Compression level of 1
    cbuf = cctx.compress(buf)

  # Decompression using a context
  var
    dctx = newDecompressCtx()
    dbuf = dctx.decompress(cbuf)

  assert(dbuf == buf)

runnableExamples:
  import std/streams
  import remizstd/[compstream]

  # Compression to a string stream
  var dest = newStringStream()
  withCompressStream(dest, cio):
    cio.write("This is some test data")

runnableExamples:
  import std/streams
  import remizstd/[compstream, decompstream]

  # Generate the test file
  let filename = "/tmp/somefile.txt"
  writeFile(filename, "This is a test file for remizstd :D")

  # Compress to a file stream
  var outFile = newFileStream("/tmp/somefile.txt.zst", fmWrite)
  assert(not isNil(outFile))

  withCompressStream(outFile, 9, cio): # Compression level 9
    cio.syncClose = true
    cio.write(readFile(filename))
    # We set cio.syncClose to true, so outFile is automatically closed as well

  # Decompress from one file stream to another file stream
  let cfilename = "/tmp/somefile.txt.zst"
  let dfilename = "/tmp/somefile.txt.orig"
  var inFile = newFileStream(cfilename, fmRead)
  outFile = newFileStream(dfilename, fmWrite)
  assert(not isNil(outFile))

  withDecompressStream(inFile, dio):
    outFile.write(dio.readAll())
  # Note we didn't set dio.syncClose to true, so we have to close our output file manually
  outFile.close()

  assert(readFile(filename) == readFile(dfilename))

runnableExamples:
  import remizstd/common

  # Normally you would load the raw dictionary data some other way,
  # like with readFile(dictionaryFilename).
  let dictBuffer = "assume this string is actually dictionary data"

  # Create the dictionary and some contexts that use it.
  var
    dict = newDict(dictBuffer, 5) # Compression level 5
    cctx = newCompressCtx(dict)
    dctx = newDecompressCtx(dict)

  # ...
  # Compress or decompress using the context or streaming API
  # ...

  echo dict.dictId()
  echo dict.memsize()

import remizstd/[common, compress, decompress, compstream, decompstream, libffi]
export common, compress, decompress, compstream, decompstream

func libVersion*(): string =
  ## Returns the current version of libzstd.
  $(version_string())
