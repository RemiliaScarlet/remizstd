# remizstd - Nim bindings for ZStandard, based on zstd.cr
# Copyright (C) 2022 Remilia Scarlet <remilia@posteo.jp>
# Copyright (C) 2019-2021 Didactic Drunk
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

## The remizstd/common module contains various core types, routines, and values
## used by the other modules, as well as the bindings for creating custom
## dictionaries.
import std/[locks, os, strformat, strutils]
import libffi

type CompressionLevel* = cint

var
  DefaultCompLevel*: CompressionLevel = if existsEnv("ZSTD_CLEVEL"): getEnv("ZSTD_CLEVEL").parseInt().cint else: 3
    ## The default compression level.  When the bindings are first loaded, this
    ## will check to see if the `ZSTD_CLEVEL` environment variable is set.  If
    ## it is, it uses the integer value from that variable.  Otherwise this
    ## defaults to 3.
  MinCompLevel*: CompressionLevel = min_c_level()
    ## The lowest supported compression level.
  MaxCompLevel*: CompressionLevel = max_c_level()
    ## The highest supported compression level.

type
  ZStdError* = object of CatchableError
    ## All errors in remizstd are of this type, or a subtype.

  CompressDictObj* = object
    ## A wraper for a `ZstdCDict` pointer.  You should use `CompressDict <#CompressDict>`_ instead.
    `ptr`*: ZstdCDict
  CompressDict* = ref CompressDictObj
    ## A managed wraper for a `ZstdCDict` pointer.

  DecompressDictObj* = object
    ## A wraper for a `ZstdDDict` pointer.  You should use `DecompressDict <#DecompressDict>`_ instead.
    `ptr`*: ZstdDDict
  DecompressDict* = ref DecompressDictObj
    ## A managed wraper for a `ZstdDDict` pointer.

  Dict* = ref object of RootObj
    ## A `Dict` is an in-memory representation of a custom dictionary.
    level: CompressionLevel
    mutex: Lock
    buf: string
    cdict: CompressDict
    ddict: DecompressDict

proc `=destroy`*(d: var CompressDictObj) =
  discard free_c_dict(d.ptr)

proc `=destroy`*(d: var DecompressDictObj) =
  discard free_d_dict(d.ptr)

proc maybeRaiseZstdError*(r: csize_t, msg: string) =
  if is_error(r) != 0:
    raise newException(ZstdError, &"{msg} returned {r} {$(get_error_name(r))}")

proc raiseZstdError*(r: csize_t, msg: string) =
  raise newException(ZstdError, &"{msg} returned {r} {$(get_error_name(r))}")

proc newDict*(buf: string, level: CompressionLevel = DefaultCompLevel): Dict =
  ## Creates a new `Dict <#Dict>`_ instance.
  Dict(buf: buf, level: level)

func dictID*(d: Dict): cuint =
  ## Returns the ID of the `Dict <#Dict>`_
  get_dict_id_from_dict(d.buf.cstring, len(d.buf).csize_t)



proc newCompressDict*(buf: string, level: CompressionLevel): CompressDict =
  ## Creates a new `CompressDict <#CompressDict>`_ instance.
  result = CompressDict()
  result.ptr = create_c_dict_by_reference(buf.cstring, len(buf).csize_t, level)

proc newCompressDict*(buf: string): CompressDict =
  ## Creates a new `CompressDict <#CompressDict>`_ instance using the
  ## `DefaultCompLevel <#DefaultCompLevel>`_.
  newCompressDict(buf, DefaultCompLevel)

func memsize*(d: CompressDict): csize_t =
  ## Returns the size of the pointer data wrapped by the `CompressDict <#CompressDict>`_.
  sizeof_c_dict(d.ptr)

proc cdict*(d: Dict): CompressDict =
  ## Returns the current `CompressDict <#CompressDict>`_ instance stored inside
  ## of the `Dict <#Dict>`_.  If the `Dict` does not currently have a
  ## `CompressDict`, then a new one will be created.
  acquire(d.mutex)
  if isNil(d.cdict):
    d.cdict = newCompressDict(d.buf, d.level)
  assert(not isNil(d.cdict))
  result = d.cdict
  release(d.mutex)


proc newDecompressDict*(buf: string): DecompressDict =
  ## Creates a new `DecmpressDict <#DecompressDict>`_ instance.
  result = DecompressDict()
  result.ptr = create_d_dict_by_reference(buf.cstring, len(buf).csize_t)

func memsize*(d: DecompressDict): csize_t =
  ## Returns the size of the pointer data wrapped by the `DecompressDict <#DecompressDict>`_.
  sizeof_d_dict(d.ptr)

proc ddict*(d: Dict): DecompressDict =
  ## Returns the current `DecompressDict <#DecompressDict>`_ instance stored
  ## inside of the `Dict <#Dict>`_.  If the `Dict` does not currently have a
  ## `DecompressDict`, then a new one will be created.
  acquire(d.mutex)
  if isNil(d.ddict):
    d.ddict = newDecompressDict(d.buf)
  assert(not isNil(d.ddict))
  result = d.ddict
  release(d.mutex)

func memsize*(d: Dict): csize_t =
  ## Returns the total size of the pointer data wrapped by the `Dict <#Dict>`_
  ## and its buffer.
  result = len(d.buf).csize_t
  if not isNil(d.cdict): result += d.cdict.memsize()
  if not isNil(d.ddict): result += d.ddict.memsize()
