# remizstd - Nim bindings for ZStandard, based on zstd.cr
# Copyright (C) 2022 Remilia Scarlet <remilia@posteo.jp>
# Copyright (C) 2019-2021 Didactic Drunk
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

## The compress module offers context-based compression.
import libffi, common

type
  CompressCtxObj* = object
    ## A wrapper and some state around `ZstdCCtx` used to compress data.  You
    ## should use `CompressCtx <#CompressCtx>`_ instead.
    freed: bool
    dict: Dict
    `ptr`*: ZstdCCtx
    level: CompressionLevel
  CompressCtx* = ref CompressCtxObj
    ## A managed context used to compress data.

proc freeCtx*(ctx: var CompressCtxObj) =
  if ctx.freed: return
  discard free_c_ctx(ctx.ptr)
  ctx.freed = true

proc freeCtx*(ctx: CompressCtx) =
  freeCtx(ctx[])

proc `=destroy`*(c: var CompressCtxObj) =
  freeCtx(c)

proc close*(c: CompressCtx) =
  freeCtx(c)



proc setDict*(ctx: CompressCtx, d: Dict) =
  ## Sets the custom dictionary on a `CompressCtx <#CompressCtx>`_.
  let r = c_ctx_ref_c_dict(ctx.ptr, d.cdict().ptr)
  maybeRaiseZstdError(r, "CompressCtx::setDict()")
  ctx.dict = d

proc newCompressCtx*(level: CompressionLevel, dict: Dict = nil): CompressCtx =
  ## Creates a new context for compressing data at the given compression level.
  ## You can optionally pass a custom dictionary to this to use for compression.
  result = CompressCtx()
  result.ptr = create_c_ctx()
  if isNil(result.ptr):
    raise newException(Defect, "NULL ptr create_c_ctx")
  result.level = level
  if not isNil(dict):
    result.setDict(dict)

proc newCompressCtx*(dict: Dict = nil): CompressCtx =
  ## Creates a new context for compressing data using the `DefaultCompLevel #<DefaultCompLevel>`_.
  ## You can optionally pass a custom dictionary to this to use for compression.
  result = CompressCtx(`ptr`: create_c_ctx(), level: DefaultCompLevel)
  if isNil(result.ptr):
    raise newException(Defect, "NULL ptr create_c_ctx")
  if not isNil(dict):
    result.setDict(dict)

proc compress*(ctx: CompressCtx, src: string): string =
  # Compresses `src` using the given context, then returns the compressed data.
  var dst = newString(compress_bound(len(src).csize_t))
  let r = compress2(ctx.ptr, dst.cstring, len(dst).csize_t, src.cstring, len(src).csize_t)
  maybeRaiseZstdError(r, "CompressCtx::compress()")
  return dst[0..<r.int]

func level*(ctx: CompressCtx): CompressionLevel =
  ## Returns the current compression level for the context.
  var ret: cint = 0
  let r = c_ctx_get_parameter(ctx.ptr, zstdCCompressionLevel, ret.addr)
  maybeRaiseZstdError(r, "CompressCtx::level()")
  ret

proc setLevel*(ctx: CompressCtx, level: CompressionLevel) =
  ## Changes the context's compression level.
  let r = c_ctx_set_parameter(ctx.ptr, zstdCCompressionLevel, level.cint)
  maybeRaiseZstdError(r, "CompressCtx::setLevel()")

func threads*(ctx: CompressCtx): int =
  ## Returns the number of threads the context will use for compression.
  var ret: cint = 0
  let r = c_ctx_get_parameter(ctx.ptr, zstdCNbWorkers, ret.addr)
  maybeRaiseZstdError(r, "CompressCtx::threads()")
  ret

proc setThreads*(ctx: CompressCtx, num: int) =
  ## Sets the number of threads the context will use for compression.
  let r = c_ctx_set_parameter(ctx.ptr, zstdCNbWorkers, num.cint)
  maybeRaiseZstdError(r, "CompressCtx::setThreads()")

func checksumFlag(ctx: CompressCtx): int =
  var ret: cint = 0
  let r = c_ctx_get_parameter(ctx.ptr, zstdCChecksumFlag, ret.addr)
  maybeRaiseZstdError(r, "CompressCtx::checksumFlag()")
  ret

proc setChecksumFlag(ctx: CompressCtx, num: int) =
  let r = c_ctx_set_parameter(ctx.ptr, zstdCChecksumFlag, num.cint)
  maybeRaiseZstdError(r, "CompressCtx::setChecksumFlag()")

func checksum*(ctx: CompressCtx): bool {.inline.} =
  ## Returns `true` if the checksum flag is set in the context, or `false`
  ## otherwise.
  ctx.checksumFlag() != 0

proc setChecksum*(ctx: CompressCtx, val: bool) {.inline.} =
  ## Changes whether or not the checkum flag is set in the context.
  if val:
    ctx.setChecksumFlag(1)
  else:
    ctx.setChecksumFlag(0)

func compressBound*(ctx: CompressCtx, size: int): int =
  ## Given data of length `size`, this returns the maximum compressed size in
  ## worst case single-pass scenario.
  let r = compress_bound(size.csize_t)
  maybeRaiseZstdError(r, "CompressCtx::compressBound()")
  r.int

func memsize*(ctx: CompressCtx): csize_t =
  ## Returns the size of the wrapped `ZstdCCtx` in memory.
  sizeof_c_ctx(ctx.ptr)
