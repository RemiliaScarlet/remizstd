# remizstd - Nim bindings for ZStandard, based on zstd.cr
# Copyright (C) 2022 Remilia Scarlet <remilia@posteo.jp>
# Copyright (C) 2019-2021 Didactic Drunk
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

## The compstream module is used to compress data into a stream.
import std/[streams, strformat]
import libffi, common, compress

var
  OutputBufferSize = c_stream_out_size().int

type
  CompressStream* = ref object of RootObj
    ## Represents a stream that uncompressed data can be written to in order to
    ## be compressed.
    ##
    ## .. note:: This does not implement the full api as found in `std/streams`.
    syncClose*: bool ## \
    ## When true, the underlying stream will be closed when `close() <#close,CompressStream>`_ is called.
    closed: bool
    obuf: string
    ctx: CompressCtx
    io: Stream

func level*(strm: CompressStream): CompressionLevel {.inline.} =
  ## Returns the current compression level for this stream.
  strm.ctx.level()

proc setLevel*(strm: CompressStream, level: CompressionLevel) {.inline.} =
  ## Sets the compression level for this stream.
  strm.ctx.setLevel(level)

func threads*(strm: CompressStream): int {.inline.} =
  ## Returns the number of threads this stream will use for compression.
  strm.ctx.threads()

proc setThreads*(strm: CompressStream, num: int) {.inline.} =
  ## Sets the number of threads this stream will use for compression.
  strm.ctx.setThreads(num)

func checksum*(strm: CompressStream): bool {.inline.} =
  ## Returns `true` if the checksum flag is used by the underlying compression
  ## context, or `false` otherwise.
  strm.ctx.checksum()

proc setChecksum*(strm: CompressStream, val: bool) {.inline.} =
  ## Sets whether or not the underlying context uses the checkum flag.
  strm.ctx.setChecksum(val)

func closed*(strm: CompressStream): bool {.inline.} =
  ## Returns `true` if the stream is closed, or `false` otherwise.  This does
  ## not consider the underlying stream.
  strm.closed

proc newCompressStream*(io: Stream, level: CompressionLevel, outputBufSize = 0): CompressStream =
  ## Creates a new stream for compression using the given compression level.
  ## Data written to this stream will be compressed and output to `io`.  If
  ## `outputBufSize` is zero, then the default output stream size as reported by
  ## libzstd will be used.
  result = CompressStream(io: io)
  result.ctx = newCompressCtx(level)
  if outputBufSize > 0:
    result.obuf = newString(outputBufSize)
  else:
    result.obuf = newString(OutputBufferSize)

proc newCompressStream*(io: Stream): CompressStream =
  ## Creates a new stream for compression using the `DefaultCompLevel <#DefaultCompLevel>`_
  ## Data written to this stream will be compressed and output to `io`.
  newCompressStream(io, DefaultCompLevel)

template withCompressStream*(io, strmVar, forms: untyped) =
  ## Creates a `CompressStream <#CompressStream>`_ bound to `strmVar` that will
  ## write compressed data to `io`, then executes `forms` inside of a block.
  ## The compression stream will use the `DefaultCompLevel <#DefaultCompLevel>`_.
  ## This ensures that `close <#close,CompressStream>`_ is called when the block
  ## exits.
  block:
    var strmVar = newCompressStream(io)
    defer: strmVar.close()
    forms

template withCompressStream*(io, level, strmVar, forms: untyped) =
  ## Creates a `CompressStream <#CompressStream>`_ bound to `strmVar` that will
  ## write compressed data to `io` using the given compression level, then
  ## executes `forms` inside of a block.  This ensures that
  ## `close <#close,CompressStream>`_ is called when the block exits.
  block:
    var strmVar = newCompressStream(io, level)
    defer: strmVar.close()
    forms

template withCompressStream*(io, level, outputBufSize, strmVar, forms: untyped) =
  ## Creates a `CompressStream <#CompressStream>`_ bound to `strmVar` that will
  ## write compressed data to `io` using the given compression level, then
  ## executes `forms` inside of a block.  This also sets the size of the output
  ## buffer.  This ensures that `close <#close,CompressStream>`_ is called when
  ## the block exits.
  block:
    var strmVar = newCompressStream(io, level, outputBufSize)
    defer: strmVar.close()
    forms

proc writeLoop(strm: CompressStream, mode: ZstdEndDirective, buf: string) =
  var
    ibuffer = ZstdInBufferSObj()
    remaining = 0
    iptr = ibuffer.addr

  if buf != "":
    ibuffer.src = buf[0].unsafeAddr
    ibuffer.size = len(buf).csize_t
    remaining = ibuffer.size.int

  while true:
    var
      obuffer = ZstdOutBufferSObj()
      optr = obuffer.addr
    obuffer.dst = strm.obuf[0].unsafeAddr
    obuffer.size = len(strm.obuf).csize_t

    let r = compress_stream2(strm.ctx.ptr, optr, iptr, mode)
    maybeRaiseZstdError(r, "CompressStream::writeLoop()")

    if obuffer.pos > 0:
      strm.io.write(strm.obuf[0..<obuffer.pos])

    case mode:
    of zstdEContinue:
      if ibuffer.pos.int == remaining: break
    of zstdEEnd:
      if r == 0: break
    else:
      raise newException(Defect, &"Unknown mode: {mode}")

proc write*(strm: CompressStream, buf: string) {.inline.} =
  ## Compresses `buf` and writes the compressed data to the underlying stream.
  strm.writeLoop(zstdEContinue, buf)

proc close*(strm: CompressStream) =
  ## Closes the compression stream.  If the `syncClose` field is true, then the
  ## underlying stream is also closed, otherwise it remains open.
  if strm.closed: return
  strm.closed = true
  strm.writeLoop(zstdEEnd, "")
  if strm.syncClose: strm.io.close()
