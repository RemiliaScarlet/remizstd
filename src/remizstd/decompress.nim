# remizstd - Nim bindings for ZStandard, based on zstd.cr
# Copyright (C) 2022 Remilia Scarlet <remilia@posteo.jp>
# Copyright (C) 2019-2021 Didactic Drunk
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

## The deccompress module offers context-based decompression.
{.overflowChecks: off.}
import libffi, common

const
  CONTENT_SIZE_UNKNOWN = 0'u64 - 1
  CONTENT_SIZE_ERROR   = 0'u64 - 2

type
  DecompressCtxObj* = object
    ## A wrapper and some state around `ZstdDCtx` used to compress data.  You
    ## should use `DecompressCtx <#DecompressCtx>`_ instead.
    freed: bool
    dict: Dict
    `ptr`*: ZstdDCtx
  DecompressCtx* = ref DecompressCtxObj
    ## A managed context used to decompress data.

  FrameSizeUnknownError* = object of ZStdError
    ## Indicates that the library cannot automatically determine the destination
    ## size.  Instead, you should use the streaming API in
    ## remizstd/decompstream.

proc freeCtx*(ctx: var DecompressCtxObj) =
  if ctx.freed: return
  discard free_d_ctx(ctx.ptr)
  ctx.freed = true

proc freeCtx*(ctx: DecompressCtx) =
  freeCtx(ctx[])

proc `=destroy`*(c: var DecompressCtxObj) =
  freeCtx(c)

proc close*(c: DecompressCtx) =
  freeCtx(c)

func dict*(c: DecompressCtx): Dict =
  c.dict


proc setDict*(ctx: DecompressCtx, d: Dict) =
  ## Sets the custom dictionary on a `DecompressCtx <#DecompressCtx>`_.
  let r = d_ctx_ref_d_dict(ctx.ptr, d.ddict().ptr)
  maybeRaiseZstdError(r, "DecompressCtx::setDict()")
  ctx.dict = d

proc newDecompressCtx*(dict: Dict = nil): DecompressCtx =
  ## Creates a new context for decompressing data.  You can optionally pass a
  ## custom dictionary to this to use for decompression.
  result = DecompressCtx(`ptr`: create_d_ctx())
  if isNil(result.ptr):
    raise newException(Defect, "NULL ptr create_d_ctx")
  if not isNil(dict):
    result.setDict(dict)

func frameContentSize*(ctx: DecompressCtx, src: string): (uint64, bool) =
  ## Attempts to determine the size of `src` after decompression.  If
  ## successful, this returns the size of `src` after decompression and `true`.
  ## If this cannot determine the size, this returns zero and `false`.
  let r = get_frame_content_size(src.cstring, len(src).csize_t)
  if r == CONTENT_SIZE_UNKNOWN:
    return (0u64, false)
  elif r == CONTENT_SIZE_ERROR:
    raiseZstdError(r.csize_t, "DecompressCtx::frameContentSize()")
  else:
    return (r.uint64, true)

proc decompress*(ctx: DecompressCtx, src: string, size = 0): string =
  ## Decompresses `src` and returns the decompressed data.  If `size` is zero,
  ## then this attempts to determine the correct size for the return value by
  ## calling `frameContentSize() <#frameContentSize,DecompressCtx,string>`_.
  ##
  ## If `size` is zero and this cannot determine the size of the return value
  ## automatically, this will raise a `FrameSizeUnknownError <#FrameSizeUnknownError>_`.
  var dst: string
  if size > 0:
    dst = newString(size)
  else:
    let (newSize, ok) = ctx.frameContentSize(src)
    if not ok:
      raise newException(FrameSizeUnknownError,
                         "Cannot automatically determine destination size, try the streaming API")
    dst = newString(newSize)

  let r = decompress_d_ctx(ctx.ptr, dst.cstring, len(dst).csize_t, src.cstring, len(src).csize_t)
  maybeRaiseZstdError(r, "DecompressCtx::decompress()")
  return dst.substr[0..<r.int]

func memsize*(ctx: DecompressCtx): csize_t =
  ## Returns the size of the wrapped `ZstdDCtx` in memory.
  sizeof_d_ctx(ctx.ptr)
