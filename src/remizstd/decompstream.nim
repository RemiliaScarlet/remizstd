# remizstd - Nim bindings for ZStandard, based on zstd.cr
# Copyright (C) 2022 Remilia Scarlet <remilia@posteo.jp>
# Copyright (C) 2019-2021 Didactic Drunk
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

## The decompstream module is used to decompress data from a stream.
import std/streams
import libffi, common, decompress

var
  InputBufferSize = c_stream_in_size().int

type
  DecompressStream* = ref object of RootObj
    ## Represents a stream that compressed data can be written to in order to be
    ## decompressed.
    ##
    ## .. note:: This does not implement the full api as found in `std/streams`.
    syncClose*: bool ## \
    ## When true, the underlying stream will be closed when `close() <#close,DecompressStream>`_ is called.
    closed: bool
    ibuf: string
    ctx: DecompressCtx
    ieof: bool
    oeof: bool
    moreOutput: bool
    ibuffer: ZstdInBufferSObj
    io: Stream

func dict*(strm: DecompressStream): Dict {.inline.} =
  ## Returns the custom dictionary assigned to the underlying compression
  ## context.
  strm.ctx.dict()

proc setDict*(strm: DecompressStream, d: Dict) {.inline.} =
  ## Sets the custom dictionary for the underlying compression context.
  strm.ctx.setDict(d)

proc newDecompressStream*(io: Stream, dict: Dict = nil, inputBufSize = 0): DecompressStream =
  ## Creates a new `DecompressStream <#DecompressStream>_`.  Data written to
  ## this stream will be decompressed and output to `io`.  If `inputBufSize` is
  ## zero, then the default input stream size as reported by libzstd will be
  ## used.  `dict` may optionally be a custom decompression dictionary.
  result = DecompressStream(io: io)
  result.ctx = newDecompressCtx()
  if inputBufSize > 0:
    result.ibuf = newString(inputBufSize)
  else:
    result.ibuf = newString(InputBufferSize)
  result.ibuffer = ZstdInBufferSObj()
  result.ibuffer.src = result.ibuf[0].unsafeAddr
  if not isNil(dict): result.ctx.setDict(dict)

template withDecompressStream*(io, strmVar, forms: untyped) =
  ## Creates a `DecompressStream <#DecompressStream>`_ bound to `strmVar` that
  ## will write decompressed data to `io`, then executes `forms` inside of a
  ## block.  This ensures that `close <#close,DecompressStream>`_ is called when
  ## the block exits.
  block:
    var strmVar = newDecompressStream(io)
    defer: strmVar.close()
    forms

template withDecompressStream*(io, inputBufSize, strmVar, forms: untyped) =
  ## Creates a `DecompressStream <#DecompressStream>`_ with a custom buffer size
  ## bound to `strmVar` that will write decompressed data to `io`, then executes
  ## `forms` inside of a block.  This ensures that `close
  ## <#close,DecompressStream>`_ is called when the block exits.
  block:
    var strmVar = newDecompressStream(io, inputBufSize = inputBufSize)
    defer: strmVar.close()
    forms

template withDecompressStream*(io, dict, inputBufSize, strmVar, forms: untyped) =
  ## Creates a `DecompressStream <#DecompressStream>`_ with a custom dictionary
  ## and buffer size bound to `strmVar` that will write decompressed data to
  ## `io`, then executes `forms` inside of a block.  This ensures that
  ## `close <#close,DecompressStream>`_ is called when the block exits.
  block:
    var strmVar = newDecompressStream(io, dict, inputBufSize)
    defer: strmVar.close()
    forms

proc setIO*(strm: DecompressStream, newStream: Stream) =
  ## Sets a new underlying stream to write decompressed data into.  This
  ## re-opens the `DecompressStream <#DecompressStream>_`.
  strm.ibuffer.pos = 0
  strm.ibuffer.size = 0
  strm.moreOutput = false
  strm.ieof = false
  strm.oeof = false
  strm.closed = false
  strm.io = newStream

proc fillBuffer(strm: DecompressStream) =
  if strm.ieof or strm.oeof or strm.moreOutput or strm.ibuffer.size > 0 and strm.ibuffer.pos != strm.ibuffer.size:
    return

  strm.ibuffer.pos = 0
  let newLen = strm.io.readData(strm.ibuf.cstring, len(strm.ibuf))
  strm.ibuffer.size = newLen.csize_t
  strm.ieof = strm.io.atEnd()

proc read*(strm: DecompressStream, num: Natural): (string, int) =
  ## Decompresses up to `num` bytes of data, then returns the decompressed data
  ## and the actual number of bytes that was decompressed.
  ##
  ## This will return an empty string and zero when the end of the compressed
  ## data is reached.
  if strm.oeof: return ("", 0)
  var
    ret = newString(num)
    obuffer = ZstdOutBufferSObj()
  obuffer.dst = ret.cstring
  obuffer.size = len(ret).csize_t

  let
    iptr = strm.ibuffer.addr
    optr = obuffer.addr

  # Feed input to decompress_stream until obuffer has data or end of input or
  # output
  while true:
    strm.fillBuffer()
    let r = decompress_stream(strm.ctx.ptr, optr, iptr)
    maybeRaiseZstdError(r, "DecompressStream::read()")
    strm.oeof = (r == 0)

    # But if `output.pos == output.size`, there might be some data left within
    # internal buffers., In which case, call ZSTD_decompressStream() again to
    # flush whatever remains in the buffer.
    strm.moreOutput = (obuffer.pos == obuffer.size)

    if strm.ieof or strm.oeof or obuffer.pos != 0:
      break

  return (ret[0..<obuffer.pos.int], obuffer.pos.int)

proc read*(strm: DecompressStream, dest: var string): int =
  ## Reads up to `len(dest)` bytes of data into `dest`, then returns the actual
  ## number of bytes that was decompressed into `dest`.
  ##
  ## This will return zero when the end of the compressed data is reached.
  var (data, ret) = strm.read(len(dest))
  dest = data
  return ret

proc readAll*(strm: DecompressStream, bufferSize: Natural = 1024 * 1024): string =
  ## Decompresses all data from the stream and returns it.
  result = ""
  while true:
    let (newBuf, ds) = strm.read(bufferSize)
    if ds == 0: break
    result.add(newBuf[0..<ds])

proc close*(strm: DecompressStream) =
  ## Closes the decompression stream.  If the `syncClose` field is `true`, then
  ## the underlying stream is also closed, otherwise it will remain open.
  if strm.closed: return
  strm.closed = true
  if strm.syncClose: strm.io.close()
