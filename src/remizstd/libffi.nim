# remizstd - Nim bindings for ZStandard, based on zstd.cr
# Copyright (C) 2022 Remilia Scarlet <remilia@posteo.jp>
# Copyright (C) 2019-2021 Didactic Drunk
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

## The libffi module implements the low-level bindings to the libzstd C library.
{.passL: "-lzstd".}

const
  ZSTD_BLOCKSIZELOG_MAX*           =             17
  ZSTD_CLEVEL_DEFAULT*             =              3
  ZSTD_MAGICNUMBER*                = 4247762216'i64
  ZSTD_MAGIC_DICTIONARY*           = 3962610743'i64
  ZSTD_MAGIC_SKIPPABLE_MASK*       = 4294967280'i64
  ZSTD_MAGIC_SKIPPABLE_START*      =      407710288
  ZSTD_VERSION_MAJOR*              =              1
  ZSTD_VERSION_MINOR*              =              4
  ZSTD_VERSION_RELEASE*            =              0
  ZstdBtlazy2*                     =          6'i64
  ZstdBtopt*                       =          7'i64
  ZstdBtultra*                     =          8'i64
  ZstdBtultra2*                    =          9'i64
  ZstdCChainLog*                   =        103'i64
  ZstdCChecksumFlag*               =        201'i64
  ZstdCCompressionLevel*           =        100'i64
  ZstdCContentSizeFlag*            =        200'i64
  ZstdCDictIdFlag*                 =        202'i64
  ZstdCEnableLongDistanceMatching* =        160'i64
  ZstdCExperimentalParam1*         =        500'i64
  ZstdCExperimentalParam2*         =         10'i64
  ZstdCExperimentalParam3*         =       1000'i64
  ZstdCExperimentalParam4*         =       1001'i64
  ZstdCExperimentalParam5*         =       1002'i64
  ZstdCHashLog*                    =        102'i64
  ZstdCJobSize*                    =        401'i64
  ZstdCLdmBucketSizeLog*           =        163'i64
  ZstdCLdmHashLog*                 =        161'i64
  ZstdCLdmHashRateLog*             =        164'i64
  ZstdCLdmMinMatch*                =        162'i64
  ZstdCMinMatch*                   =        105'i64
  ZstdCNbWorkers*                  =        400'i64
  ZstdCOverlapLog*                 =        402'i64
  ZstdCSearchLog*                  =        104'i64
  ZstdCStrategy*                   =        107'i64
  ZstdCTargetLength*               =        106'i64
  ZstdCWindowLog*                  =        101'i64
  ZstdDExperimentalParam1*         =       1000'i64
  ZstdDWindowLogMax*               =        100'i64
  ZstdDfast*                       =          2'i64
  ZstdEContinue*                   =          0'i64
  ZstdEEnd*                        =          2'i64
  ZstdEFlush*                      =          1'i64
  ZstdFast*                        =          1'i64
  ZstdGreedy*                      =          3'i64
  ZstdLazy*                        =          4'i64
  ZstdLazy2*                       =          5'i64
  ZstdResetParameters*             =          2'i64
  ZstdResetSessionAndParameters*   =          3'i64
  ZstdResetSessionOnly*            =          1'i64

type
  ZstdBoundsObj* {.importc: "struct ZSTD_bounds", header: "zstd.h".} = object
    error*: csize_t
    lower_bound*: cint
    upper_bound*: cint

  ZstdInBufferSObj* {.importc: "struct ZSTD_inBuffer_s", header: "zstd.h".} = object
    src*: pointer
    size*: csize_t
    pos*: csize_t

  ZstdOutBufferSObj* {.importc: "struct ZSTD_outBuffer_s", header: "zstd.h".} = object
    dst*: pointer
    size*: csize_t
    pos*: csize_t

  ZstdCCtx* = pointer
  ZstdCDict* = pointer
  ZstdDCtx* = pointer
  ZstdDDict* = pointer
  ZstdInBuffer* = ptr ZstdInBufferSObj
  ZstdOutBuffer* = ptr ZstdOutBufferSObj

  ZstdCCtxS* = void
  ZstdCDictS* = void
  ZstdCStream* = ZstdCCtx
  ZstdDCtxS* = void
  ZstdDDictS* = void
  ZstdDStream* = ZstdDCtx

  ZstdCParameter* = enum
    zstdCExperimentalParam2         =   10
    zstdCCompressionLevel           =  100
    zstdCWindowLog                  =  101
    zstdCHashLog                    =  102
    zstdCChainLog                   =  103
    zstdCSearchLog                  =  104
    zstdCMinMatch                   =  105
    zstdCTargetLength               =  106
    zstdCStrategy                   =  107
    zstdCEnableLongDistanceMatching =  160
    zstdCLdmHashLog                 =  161
    zstdCLdmMinMatch                =  162
    zstdCLdmBucketSizeLog           =  163
    zstdCLdmHashRateLog             =  164
    zstdCContentSizeFlag            =  200
    zstdCChecksumFlag               =  201
    zstdCDictIdFlag                 =  202
    zstdCNbWorkers                  =  400
    zstdCJobSize                    =  401
    zstdCOverlapLog                 =  402
    zstdCExperimentalParam1         =  500
    zstdCExperimentalParam3         = 1000
    zstdCExperimentalParam4         = 1001
    zstdCExperimentalParam5         = 1002

  ZstdDParameter* = enum
    zstdDWindowLogMax       =  100
    zstdDExperimentalParam1 = 1000

  ZstdEndDirective* = enum
    zstdEContinue = 0
    zstdEFlush    = 1
    zstdEEnd      = 2

  ZstdResetDirective* = enum
    zstdResetSessionOnly          = 1
    zstdResetParameters           = 2
    zstdResetSessionAndParameters = 3

proc c_ctx_load_dictionary*(cctx: ZstdCCtx, dict: pointer, dict_size: csize_t): csize_t {.importc: "ZSTD_CCtx_loadDictionary", header: "zstd.h"}
proc c_ctx_ref_c_dict*(cctx: ZstdCCtx, cdict: ZstdCDict): csize_t {.importc: "ZSTD_CCtx_refCDict", header: "zstd.h".}
proc c_ctx_ref_prefix*(cctx: ZstdCCtx, prefix: pointer, prefix_size: csize_t): csize_t {.importc: "ZSTD_CCtx_refPrefix", header: "zstd.h".}
proc c_ctx_reset*(cctx: ZstdCCtx, reset: ZstdResetDirective): csize_t {.importc: "ZSTD_CCtx_reset", header: "zstd.h".}
proc c_ctx_set_parameter*(cctx: ZstdCCtx, param: ZstdCParameter, value: cint): csize_t {.importc: "ZSTD_CCtx_setParameter", header: "zstd.h".}
proc c_ctx_get_parameter*(cctx: ZstdCCtx, param: ZstdCParameter, value: ptr cint): csize_t {.importc: "ZSTD_CCtx_getParameter", header: "zstd.h".}
proc c_ctx_set_pledged_src_size*(cctx: ZstdCCtx, pledged_src_size: culonglong): csize_t {.importc: "ZSTD_CCtx_setPledgedSrcSize", header: "zstd.h".}

proc c_param_get_bounds*(c_param: ZstdCParameter): ptr ZstdBoundsObj {.importc: "ZSTD_cParam_getBounds", header: "zstd.h".}

proc c_stream_in_size*(): csize_t {.importc: "ZSTD_CStreamInSize", header: "zstd.h".}
proc c_stream_out_size*(): csize_t {.importc: "ZSTD_CStreamOutSize", header: "zstd.h".}

proc compress*(dst: pointer, dst_capacity: csize_t, src: pointer, src_size: csize_t, compression_level: cint): csize_t {.importc: "ZSTD_compress", header: "zstd.h".}
proc compress2*(cctx: ZstdCCtx, dst: pointer, dst_capacity: csize_t, src: pointer, src_size: csize_t): csize_t {.importc: "ZSTD_compress2", header: "zstd.h".}
proc compress_bound*(src_size: csize_t): csize_t {.importc: "ZSTD_compressBound", header: "zstd.h".}
proc compress_c_ctx*(cctx: ZstdCCtx, dst: pointer, dst_capacity: csize_t, src: pointer, src_size: csize_t, compression_level: cint): csize_t {.importc: "ZSTD_compressCCtx", header: "zstd.h".}
proc compress_stream*(zcs: ptr ZstdCStream, output: ZstdOutBuffer, input: ZstdInBuffer): csize_t {.importc: "ZSTD_compressStream", header: "zstd.h".}
proc compress_stream2*(cctx: ZstdCCtx, output: ZstdOutBuffer, input: ZstdInBuffer, end_op: ZstdEndDirective): csize_t {.importc: "ZSTD_compressStream2", header: "zstd.h".}
proc compress_using_c_dict*(cctx: ZstdCCtx, dst: pointer, dst_capacity: csize_t, src: pointer, src_size: csize_t, cdict: ZstdCDict): csize_t {.importc: "ZSTD_compress_usingCDict", header: "zstd.h".}
proc compress_using_dict*(ctx: ZstdCCtx, dst: pointer, dst_capacity: csize_t, src: pointer, src_size: csize_t, dict: pointer, dict_size: csize_t, compression_level: cint): csize_t {.importc: "ZSTD_compress_usingDict", header: "zstd.h".}

proc create_c_ctx*(): ZstdCCtx {.importc: "ZSTD_createCCtx", header: "zstd.h".}
proc create_c_dict*(dict_buffer: pointer, dict_size: csize_t, compression_level: cint): ZstdCDict {.importc: "ZSTD_createCDict", header: "zstd.h".}
proc create_c_dict_by_reference*(dict_buffer: pointer, dict_size: csize_t, compression_level: cint): ZstdCDict {.importc: "ZSTD_createCDict_byReference", header: "zstd.h".}
proc create_c_stream*(): ptr ZstdCStream {.importc: "ZSTD_createCStream", header: "zstd.h".}
proc create_d_ctx*(): ZstdDCtx {.importc: "ZSTD_createDCtx", header: "zstd.h".}
proc create_d_dict*(dict_buffer: pointer, dict_size: csize_t): ZstdDDict {.importc: "ZSTD_createDDict", header: "zstd.h".}
proc create_d_dict_by_reference*(dict_buffer: pointer, dict_size: csize_t): ZstdDDict {.importc: "ZSTD_createDDict_byReference", header: "zstd.h".}
proc create_d_stream*(): ZstdDStream {.importc: "ZSTD_createDStream", header: "zstd.h".}

proc d_ctx_load_dictionary*(dctx: ZstdDCtx, dict: pointer, dict_size: csize_t): csize_t {.importc: "ZSTD_DCtx_loadDictionary", header: "zstd.h".}
proc d_ctx_ref_d_dict*(dctx: ZstdDCtx, ddict: ZstdDDict): csize_t {.importc: "ZSTD_DCtx_refDDict", header: "zstd.h".}
proc d_ctx_ref_prefix*(dctx: ZstdDCtx, prefix: pointer, prefix_size: csize_t): csize_t {.importc: "ZSTD_DCtx_refPrefix", header: "zstd.h".}
proc d_ctx_reset*(dctx: ZstdDCtx, reset: ZstdResetDirective): csize_t {.importc: "ZSTD_DCtx_reset", header: "zstd.h".}
proc d_ctx_set_parameter*(dctx: ZstdDCtx, param: ZstdDParameter, value: cint): csize_t {.importc: "ZSTD_DCtx_setParameter", header: "zstd.h".}

proc d_param_get_bounds*(d_param: ZstdDParameter): ptr ZstdBoundsObj {.importc: "ZSTD_dParam_getBounds", header: "zstd.h".}

proc d_stream_in_size*(): csize_t {.importc: "ZSTD_DStreamInSize", header: "zstd.h".}
proc d_stream_out_size*(): csize_t {.importc: "ZSTD_DStreamOutSize", header: "zstd.h".}

proc decompress*(dst: pointer, dst_capacity: csize_t, src: pointer, compressed_size: csize_t): csize_t {.importc: "ZSTD_decompress", header: "zstd.h".}
proc decompress_d_ctx*(dctx: ZstdDCtx, dst: pointer, dst_capacity: csize_t, src: pointer, src_size: csize_t): csize_t {.importc: "ZSTD_decompressDCtx", header: "zstd.h".}
proc decompress_stream*(zds: ZstdDStream, output: ZstdOutBuffer, input: ZstdInBuffer): csize_t {.importc: "ZSTD_decompressStream", header: "zstd.h".}
proc decompress_using_d_dict*(dctx: ZstdDCtx, dst: pointer, dst_capacity: csize_t, src: pointer, src_size: csize_t, ddict: ZstdDDict): csize_t {.importc: "ZSTD_decompress_usingDDict", header: "zstd.h".}
proc decompress_using_dict*(dctx: ZstdDCtx, dst: pointer, dst_capacity: csize_t, src: pointer, src_size: csize_t, dict: pointer, dict_size: csize_t): csize_t {.importc: "ZSTD_decompress_usingDict", header: "zstd.h".}

proc end_stream*(zcs: ptr ZstdCStream, output: ptr ZstdOutBuffer): csize_t {.importc: "ZSTD_endStream", header: "zstd.h".}

proc find_frame_compressed_size*(src: pointer, src_size: csize_t): csize_t {.importc: "ZSTD_findFrameCompressedSize", header: "zstd.h".}

proc flush_stream*(zcs: ptr ZstdCStream, output: ptr ZstdOutBuffer): csize_t {.importc: "ZSTD_flushStream", header: "zstd.h".}

proc free_c_ctx*(cctx: ZstdCCtx): csize_t {.importc: "ZSTD_freeCCtx", header: "zstd.h".}
proc free_c_dict*(c_dict: ZstdCDict): csize_t {.importc: "ZSTD_freeCDict", header: "zstd.h".}
proc free_c_stream*(zcs: ZstdCStream): csize_t {.importc: "ZSTD_freeCStream", header: "zstd.h".}
proc free_d_ctx*(dctx: ZstdDCtx): csize_t {.importc: "ZSTD_freeDCtx", header: "zstd.h".}
proc free_d_dict*(ddict: ZstdDDict): csize_t {.importc: "ZSTD_freeDDict", header: "zstd.h".}
proc free_d_stream*(zds: ZstdDStream): csize_t {.importc: "ZSTD_freeDStream", header: "zstd.h".}

proc get_decompressed_size*(src: pointer, src_size: csize_t): culonglong {.importc: "ZSTD_getDecompressedSize", header: "zstd.h".}
proc get_dict_id_from_d_dict*(ddict: ZstdDDict): cuint {.importc: "ZSTD_getDictID_fromDDict", header: "zstd.h".}
proc get_dict_id_from_dict*(dict: pointer, dict_size: csize_t): cuint {.importc: "ZSTD_getDictID_fromDict", header: "zstd.h".}
proc get_dict_id_from_frame*(src: pointer, src_size: csize_t): cuint {.importc: "ZSTD_getDictID_fromFrame", header: "zstd.h".}
proc get_error_name*(code: csize_t): cstring {.importc: "ZSTD_getErrorName", header: "zstd.h".}
proc get_frame_content_size*(src: pointer, src_size: csize_t): culonglong {.importc: "ZSTD_getFrameContentSize", header: "zstd.h".}

proc init_c_stream*(zcs: ZstdCStream, compression_level: cint): csize_t {.importc: "ZSTD_initCStream", header: "zstd.h".}
proc init_d_stream*(zds: ZstdDStream): csize_t {.importc: "ZSTD_initDStream", header: "zstd.h".}

proc is_error*(code: csize_t): cuint {.importc: "ZSTD_isError", header: "zstd.h".}

proc max_c_level*(): cint {.importc: "ZSTD_maxCLevel", header: "zstd.h".}
proc min_c_level*(): cint {.importc: "ZSTD_minCLevel", header: "zstd.h".}

proc sizeof_c_ctx*(cctx: ZstdCCtx): csize_t {.importc: "ZSTD_sizeof_CCtx", header: "zstd.h".}
proc sizeof_c_dict*(cdict: ZstdCDict): csize_t {.importc: "ZSTD_sizeof_CDict", header: "zstd.h".}
proc sizeof_c_stream*(zcs: ZstdCStream): csize_t {.importc: "ZSTD_sizeof_CStream", header: "zstd.h".}
proc sizeof_d_ctx*(dctx: ZstdDCtx): csize_t {.importc: "ZSTD_sizeof_DCtx", header: "zstd.h".}
proc sizeof_d_dict*(ddict: ZstdDDict): csize_t {.importc: "ZSTD_sizeof_DDict", header: "zstd.h".}
proc sizeof_d_stream*(zds: ZstdDStream): csize_t {.importc: "ZSTD_sizeof_DStream", header: "zstd.h".}

proc version_number*(): cuint {.importc: "ZSTD_versionNumber", header: "zstd.h".}
proc version_string*(): cstring {.importc: "ZSTD_versionString", header: "zstd.h".}
