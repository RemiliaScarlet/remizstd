import unittest
import remizstd

proc contextsWithBufs(): (CompressCtx, DecompressCtx, string, string) =
  var
    buf = newString(128)
    dbuf2 = newString(len(buf))
    cctx = newCompressCtx(1)
    dctx = newDecompressCtx()

  buf[len(buf) div 2] = chr(1)
  (cctx, dctx, buf, dbuf2)

suite "(De)compression Contexts":
  test "Simple API compress/decompress with auto buffer":
    var (cctx, dctx, buf, _) = contextsWithBufs()

    cctx.setLevel(1)
    check(cctx.level() == 1)

    var cbuf = cctx.compress(buf)
    check(cctx.level() == 1)
    var dbuf = dctx.decompress(cbuf)
    check(len(cbuf) < len(dbuf))
    check(dbuf == buf)

  test "Simple API compress/decompress with destination buffer":
    var (cctx, dctx, buf, dbuf2) = contextsWithBufs()
    var cbuf = cctx.compress(buf)
    var dbuf = dctx.decompress(cbuf, len(dbuf2))
    check(len(cbuf) < len(dbuf))
    check(dbuf == buf)

  test "Simple API compress/decompress raises with small destination buffer":
    var (cctx, dctx, buf, _) = contextsWithBufs()
    var cbuf = cctx.compress(buf)
    expect(ZStdError):
      var dbuf = dctx.decompress(cbuf, 1)

  test "Simple API compress/decompress with checksums":
    var (cctx, dctx, buf, dbuf2) = contextsWithBufs()
    cctx.setLevel(1)
    var cbuf = cctx.compress(buf)
    cctx.setChecksum(true)
    var cbuf2 = cctx.compress(buf)

    check(cctx.checksum() == true)
    check(len(cbuf2) == (len(cbuf) + 4))
    var dbuf = dctx.decompress(cbuf2)
    check(dbuf == buf)

  test "memsizes":
    var (cctx, dctx, _, _) = contextsWithBufs()
    check(cctx.memsize() > 0)
    check(dctx.memsize() > 0)
