import std/[strutils, unittest]
import remizstd

suite "Dicts":
  setup:
    var testDictBuf = readFile("tests/test.9.dictionary")

  test "Cannot decompress without the correct dictionary":
    var dict = newDict(testDictBuf, 9)
    var cctx = newCompressCtx(dict)
    var dctx = newDecompressCtx()
    var cbuf = cctx.compress("foo")

    try:
      discard dctx.decompress(cbuf)
      checkpoint("Decompression succeeded when it shouldn't have")
      fail()
    except ZStdError as err:
      check(err.msg.contains("Dictionary mismatch") == true)

    dctx = newDecompressCtx(dict)
    discard dctx.decompress(cbuf)

  test "Test dictId() and memsize()":
    var dict = newDict(testDictBuf, 9)
    var id = dict.dictId()
    check(id != 0)

    var ms = dict.memsize()
    check(ms > 0)

    discard dict.cdict()
    var cms = dict.memsize
    check(cms > ms) # Memsize grows as dicts are lazily loaded.

    discard dict.ddict()
    var dms = dict.memsize
    check(dms > cms) # Memsize grows as dicts are lazily loaded.
