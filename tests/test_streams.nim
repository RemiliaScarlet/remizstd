import std/[random, streams, unittest]
import remizstd

suite "(De)compression Streams":
  test "Streaming API compress/decompress":
    var buf = newString(128)
    buf[len(buf) div 2] = 255.char

    var mio = newStringStream()
    withCompressStream(mio, cio):
      cio.setLevel(1)
      for i in 0..<len(buf):
        cio.write(buf[i..<(i + 1)])
    var cbuf = mio.data
    check(len(cbuf) < len(buf))

    # read with large buf
    mio = newStringStream(cbuf)
    withDecompressStream(mio, dio):
      var dbuf = newString(len(buf) * 2)
      var dsize = dio.read(dbuf)
      check(dsize == len(buf))
      dbuf = dbuf[0..<dsize]
      check(buf == dbuf)

      # 2nd read at eof
      (dbuf, dsize) = dio.read(128)
      check(dsize == 0)

    # read with small buf
    mio = newStringStream(cbuf)
    withDecompressStream(mio, dio):
      var dbuf = newString(len(buf) div 2)
      var dsize = 0
      for i in 0..<2:
        var ds = dio.read(dbuf)
        dsize += ds
        check(ds == len(dbuf))
      check(dsize == len(buf))

      # 3rd read at eof
      var dbuf2 = newString(128)
      dsize = dio.read(dbuf2)
      check(dsize == 0)

    # # read single byte at a time
    mio = newStringStream(cbuf)
    withDecompressStream(mio, dio):
      var dbuf = newString(1)
      var dbuf2 = newString(len(buf))
      var dsize = 0

      for i in 0..<len(buf):
        let ds = dio.read(dbuf)
        dsize += ds
        check(ds == len(dbuf))
        dbuf2[i] = dbuf[0]
      check(dsize == len(buf))
      check(dbuf2 == buf)

      # last read at eof
      dsize = dio.read(dbuf2)
      check(dsize == 0)

  test "Streaming API compress/decompress with large data":
    var rnd = initRand(0)
    var buf = newString(10 * 1024 * 1024)

    for i in 0..<len(buf):
      buf[i] = rnd.rand(255).char

    var mio = newStringStream()
    withCompressStream(mio, cio):
      for i in 0..<len(buf):
        cio.write(buf[i..<(i + 1)])
    var cbuf = mio.data

    # +|- 2%
    check(abs(len(cbuf) - len(buf)).abs().float64 < (len(buf).float64 * 0.02))

    # read with large buf
    mio = newStringStream(cbuf)
    withDecompressStream(mio, dio):
      var dbuf = ""
      var dsize = 0
      while true:
        let (newBuf, ds) = dio.read(len(buf) * 2)
        if ds == 0: break
        dsize += ds
        dbuf.add(newBuf)
      check(dsize == len(buf))
      check(dbuf == buf)

  test "Decompress zstd compressed file":
    var fio = newFileStream("./tests/foo.9.txt.zstd", fmRead)
    withDecompressStream(fio, dio):
      let buf = dio.readAll()
      check(buf[0..<6] == "foobaz")
      check(len(buf) == 420946)
